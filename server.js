#!/usr/bin/env nodejs
var express = require("express");
var logger = require('morgan');
var fs = require("fs");
var config = require("./config");

console.log("Init app...");
var app = express();
app.use(logger('combined'));
app.set("views", __dirname + "/views");

if (config.prod) {
	console.log("Running in PRODUCTION mode");
}

console.log("Enabling minification engine...");
var minify = require("express-minify");
var cachedir = __dirname + "/cache";
if (!fs.existsSync(cachedir)) fs.mkdirSync(cachedir);

express.static.mime.define(
{
    'text/coffeescript':  ['coffee'],
    'text/less':          ['less'],
    'text/x-scss':        ['scss'],
    'text/stylus':        ['styl']
});

app.use('/static', minify({
	cache: cachedir
}));
app.use('/static', express.static(__dirname + "/static"));

console.log("Setting up view engine...");
var hbs = require("express-handlebars");
app.engine("hbs", hbs({}));
app.set("view engine", "hbs");
app.locals.layout = "default.hbs";

console.log("Adding routes...");
var controllerPath = __dirname + "/controllers/";
fs.readdirSync(controllerPath).forEach(function(file){
	var path = controllerPath + file.substr(0, file.indexOf("."));
	//console.log(path);
	require(path)(app);
});

console.log("Starting...");
var server = app.listen(config.port, function(){
	var lhost = server.address().address;
	var lport = server.address().port;

	console.log("Server listening on http://%s:%s", lhost, lport);
});
