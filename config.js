var args = require("minimist")(process.argv.slice(2));

module.exports = {
	port: args.port || 8080,
	prod: args.prod || false,
};